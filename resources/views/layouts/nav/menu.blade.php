<div class="row">
	<div class="col-sm-12">
		<nav class="navbar navbar-light bg-blue">
			<div class="col-sm-3"></div>
			<div class="col-sm-7">
				@foreach(config('menu.menu') as $menu)
					<a href="{{url($menu['link'])}}" class="navbar-brand {{Helper::setActive($menu['active'],'active-link')}}">{{$menu['title']}}</a>
				@endforeach
			</div>
			<div class="col-sm-2"></div>
		</nav>
	</div>
</div>

